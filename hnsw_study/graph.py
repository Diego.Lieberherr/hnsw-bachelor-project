import numpy as np
import heapq

class Graph:
    """
    Class to represent a graph in the HNSW algorithm
    """
    def __init__(self, M, Mmax, select_mode="heuristic"):
        """
        Initialize the graph

        Parameters:
        M (int): number of neighbors added when a node is inserted
        Mmax (int): maximum number of neighbors per node

        Extra informations:
        graph structure is stored inside the nodes attribute as an adjacency list
        nodes data are stored inside the data attribute
        """
        self.nodes = {}
        self.data = {}
        self.M = M
        self.Mmax = Mmax
        self.select_mode = select_mode
        
    def insert(self, node, attr, eps, ef):
        """
        Insert a new node into the graph

        Parameters:
        node (int): the ID of the node to be inserted
        attr (numpy array): the data attribute of the node to be inserted
        eps (list): list of the ID of the nodes used as entry points
        ef (int): size of the dynamic neighbor list
        """
        if all(ep in self.nodes for ep in eps):
            self.nodes[node] = set()
            self.data[node] = attr
            candidates = self.search(attr, eps, ef)
            neighbors = self._select_neighbors(node, candidates, self.M, mode=self.select_mode)
            self._connect_nodes(node, neighbors)
            return candidates
        else:
            self.nodes[node] = set()
            self.data[node] = attr

    def search(self, q, eps, ef):
        """
        Search for the nearest neighbors of a query node
        
        Parameters:
        q (numpy array): the data attribute of the query node
        eps (list): list of the ID of the nodes used as entry points
        ef (int): size of the dynamic neighbor list

        Returns:
        list: list of nearest neighbors, each neighbor is represented as a tuple (distance to query, neighborID)

        Extra informations:
        a heap queue is used to keep the potential neighbors (candidates) and the nearest neighbors found (results) ordered at all time
        """
        candidates = []
        results = []
        visited = set()
        for ep in eps:
            visited.add(ep)
            d_ep = self._dist(self.data[ep], q)
            heapq.heappush(candidates, (d_ep, ep))
            heapq.heappush(results, (-d_ep, ep))
        while candidates:
            d, c = heapq.heappop(candidates)
            if d > -results[0][0]:
                #closest candidate is farther than the farthest neighbor
                break
            for n in self.nodes[c]:
                if n not in visited:
                    visited.add(n)
                    d = self._dist(self.data[n], q)
                    if d < -results[0][0] or len(results) < ef:
                        #add current candidate to the neighbor list
                        heapq.heappush(candidates, (d, n))
                        heapq.heappush(results, (-d, n))
                        if len(results) > ef:
                            #remove the farthest neighbor when the dynamic list is full
                            heapq.heappop(results)
        return [(-i[0], i[1]) for i in heapq.nlargest(len(results), results)]
    
    def _dist(self, n, q):
        """
        Compute the squared Euclidean distance between two node
        
        Parameters:
        n (numpy array): the data attribute of the first node
        q (numpy array): the data attribute of the second node

        Returns:
        float: squared Euclidean distance
        """
        diff = n - q
        return np.dot(diff, diff)

    def _select_neighbors(self, node, candidates, M, mode="heuristic"):
        """
        Select M neighbors out of the potential candidates,
        mode defines which selection method to use

        Parameters:
        node (int): the ID of the node
        candidates (list): list of the nearest neighbors found
        M (int): number of nearest neighbors to select
        mode (string): selection method to use, currently accepts "heuristic" or "simple" methods

        Returns:
        list: list containing the ids of the M nearest neighbors selected
        """

        #selection using heuristic
        if mode == "heuristic":
            candidates_ids = [c[1] for c in candidates]
            neighbors_ids = [self.nodes[c] for c in candidates_ids]
            neighbors_ids = set.union(*neighbors_ids) | set(candidates_ids)
            current_candidates = []
            to_connect = []
            discarded = []
            heapq.heapify(current_candidates)
            
            #extend candidates list with their neighbors
            for n in neighbors_ids:
                if n != node:
                    d = self._dist(self.data[node], self.data[n])
                    heapq.heappush(current_candidates, (d, n))

            #use heuristic to select neighbors for better connectivity
            while ((current_candidates) and (len(to_connect) < M)):
                d, c = heapq.heappop(current_candidates)
                if any(d < n[0] for n in to_connect) or not to_connect:
                    to_connect += [(d, c)]
                else:
                    discarded += [(d, c)]
            
            #add connections until reaching M connections (if necessary)
            while ((discarded) and (len(to_connect) < M)):
                d, c = discarded.pop(0)
                to_connect += [(d, c)]
        
            return [r[1] for r in to_connect]
        
        #selection using M nearest neighbors
        elif mode == "simple":
            to_connect = []
            while ((candidates) and (len(to_connect) < M)):
                _, c = candidates.pop(0)
                to_connect += [c]
            return to_connect
        
        #other selection methods can be added here
        else:
            pass

    def _connect_nodes(self, node, neighbors):
        """
        Connect a node to its nearest neighbors, 
        also prune the excess connection if needed (if number of connections exceed Mmax)

        Parameters:
        node (int): the ID of the node
        neighbors (list): list containing the ids of the nearest neighbors
        """

        #add connections
        for n in neighbors:
            self.nodes[node].add(n)
            self.nodes[n].add(node)

        #prune excess connections if needed, reselect the neighbors to connect by using the select_neighbors method
        for n in neighbors:
            if len(self.nodes[n]) > self.Mmax:
                candidates = [(self._dist(self.data[n], self.data[n_neighbors]), n_neighbors) for n_neighbors in self.nodes[n]]
                new_neighbors = set(self._select_neighbors(n, candidates, self.Mmax, mode=self.select_mode))
                to_modify = new_neighbors^self.nodes[n]
                for m in to_modify:
                    if m in self.nodes[n]:
                        self.nodes[n].remove(m)
                        self.nodes[m].remove(n)
                    else:
                        self.nodes[n].add(m)
                        self.nodes[m].add(n)
