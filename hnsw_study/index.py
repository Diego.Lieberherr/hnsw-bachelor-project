import numpy as np
from .graph import Graph

class Index:
    """
    Class to represent the HNSW index
    """
    def __init__(self, M=16, ef_construction=100, select_mode="heuristic"):
        """
        Initialize the index

        Parameters:
        M (int): number of neighbors added when a node is inserted
        ef_construction (int): size of the dynamic neighbor list during construction

        Extra informations:
        values of Mmax, Mmax0 and mL parameters follows the convention used in hnswlib
        """
        self.ef_construction = max(ef_construction, M)
        self.M = M
        self.Mmax = M
        self.Mmax0 = 2 * M
        self.mL = 1 / np.log(M)
        self.entry_point = None
        self.select_mode = select_mode
        self.graphs = []
        self.path = []
    
    def add(self, data, build_graph=True):
        """
        Add data to the HNSW index

        Parameters:
        data (numpy array): The dataset to be added to the index

        """
        layers = np.floor((-np.log(np.random.uniform(0, 1, data.shape[0]))) * self.mL).astype(int)
        self.entry_point = 0
        max_layer = np.max(layers)
        unique, counts = np.unique(layers, return_counts=True)
        self.layer_count = dict(zip(unique, counts))

        for i in range(max_layer + 1):
            #initialize graphs for all layers
            if i == 0:
                Mmax = self.Mmax0
            else:
                Mmax = self.Mmax
            g = Graph(self.M, Mmax, select_mode=self.select_mode)
            self.graphs.append(g)

        for i in range(len(layers)):
            #insert node i at layer l
            #l_ep is the layer of the current entrypoint

            ep = [self.entry_point]
            l = layers[i]
            l_ep = layers[self.entry_point]

            #perform search at the upper layers (from l_ep to l)
            for j in range(l_ep, l, -1):
                if not build_graph:
                    continue

                ep = [self.graphs[j].search(data[i], ep, 1)[0][1]]

            #insert node at layer l and the lower layers (from l to layer 0)    
            for j in range(l, -1, -1):
                if not build_graph:
                    self.graphs[j].nodes[i] = set()
                    self.graphs[j].data[i] = data[i]
                    continue
                candidates = self.graphs[j].insert(i, data[i], ep, self.ef_construction)
                if candidates:
                    ep = [c[1] for c in candidates]
            
            if l > l_ep:
                self.entry_point = i
    
    def search(self, q, ef=10, k=1):
        """
        Search for the nearest neighbors of a query point

        Parameters:
        q (numpy array): The data attribute of the query point
        ef (int): size of the dynamic neighbor list
        k (int): number of nearest neighbors to return

        Returns:
        list: list of nearest neighbors, each neighbor is represented as a tuple (distance to query, neighborID)
        """
        ep = [self.entry_point]
        self.path = []
        self.path.append((self.entry_point, self.graphs[0]._dist(self.graphs[0].data[self.entry_point], q)))

        for g in reversed(self.graphs):
            candidates = g.search(q, ep, ef)
            ep = [c[1] for c in candidates]
            self.path.append((candidates[0][1], candidates[0][0]))

        return map(list, zip(*candidates[:k]))
    
    def print_query_path(self):
        """
        Print the path of the last query
        """
        if self.path:
            print("Path of last query:")
            for i in range(len(self.graphs)):
                print(f"Layer {len(self.graphs) - i - 1} (node {self.path[i][0]} -> node {self.path[i + 1][0]}) distance gained {self.path[i][1] - self.path[i + 1][1]}")
