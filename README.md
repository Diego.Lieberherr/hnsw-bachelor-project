# HNSW Study Library

This library is part of a bachelor project aimed at studying the Hierarchical Navigable Small World (HNSW) algorithm to understand why it performs well despite the curse of dimensionality.

## Purpose
The primary focus of this project is to analyze the HNSW algorithm rather than to optimize for performance or memory usage. For a more optimized implementation, please refer to [hnswlib](https://github.com/nmslib/hnswlib/).

## Installation
To install the library, follow these steps:
```
apt-get install -y python-setuptools python-pip
git clone https://gitlab.unige.ch/Diego.Lieberherr/hnsw-bachelor-project.git
cd hnsw-bachelor-project
pip install .
```
## Description
The library implements an index structure based on the HNSW algorithm. For now only squared L2 distance is supported. Below are the key parameters:
- `M`: The maximum number of neighbors each node can have.
- `ef_construction`: The size of the dynamic list for the nearest neighbors during the construction of the index. Bigger `ef_construction` leads to longer construction, but better index quality. At some point, increasing `ef_construction` does not improve the quality of the index.
- `select_mode`: The method used for neighbors selection during the construction of the index. The simple method select the `M` nearest neighbors when connecting a new node. The heuristic method select neighbors to improve the overall connectivity of the graph.
- `ef_search` or `ef`: The size of the dynamic list for the nearest neighbors during the search operation. `ef` cannot be set lower than the number of queried nearest neighbors `k`. The value of ef can be anything between `k` and the size of the dataset.
- `k`: The number of nearest neighbors to return in the search operation.

### Index Class
* `hnsw_study.Index(M=16, ef_construction=100, select_mode="heuristic")`: Initialize an empty index
    * `M` defines the maximum number of connections in graphs
    * `ef_construction` defines the size of the dynamic list used during graph construction
    * `select_mode` defines the selection method used during construction (`"heuristic"` or `"simple"`)

`hnsw_study.Index` methods:

* `add(data)`: Adds data points to the index.
* `search(q, ef, k=1)`: Performs a nearest neighbor search for query `q` with the specified `ef` and returns the `k` nearest neighbors.
* `print_query_path()`: Prints the path taken during the last query operation.

properties of `hnsw_study.Index` class:

* `ef_construction` - size of the dynamic list used during construction
* `M` - number of bidirectional connections made when inserting a new node
* `Mmax` - maximum number of bidirectional connections a node can have in all layer except layer 0
* `Mmax0` - maximum number of bidirectional connections a node can have in layer 0
* `mL` - normalization factor of the probability distribution used in layer selection
* `entry_point` - entry point of the top layer
* `select_mode` - method for selecting neighbors during construction
* `graphs` - list containing the created graphs, ordered from layer 0 to the top layer 

### Graph Class
This class is used internally by the Index.
* `hnsw_study.Graph(M, Mmax, select_mode="heuristic")`: Initialize an empty graph
    * `M` defines the number of bidirectional connections made when inserting a new node
    * `Mmax` defines the maximum number of bidirectional connections a node can have
    * `select_mode` defines the selection method used during insertion (`"heuristic"` or `"simple"`)

`hnsw_study.Graph` methods:

* `insert(self, node, attr, eps, ef)`: insert a new node in the graph
    * `node` defines the id of the new node to be inserted
    * `attr` defines the attributes of the new node to be inserted
    * `eps` defines which entry points to use for the insertion process
    * `ef` defines the size of the dynamic list used for insertion process
* `search(q, eps, ef)` : search process
    * `q` defines the query point
    * `eps` defines which entry points to use for the search process
    * `ef` defines the size of the dynamic list used for search process

properties of `hnsw_study.Graph` class:

* `nodes` - graph adjacency list
* `data` - attributes of all nodes contained in the graph
* `M` - number of bidirectional connections made when inserting a new node
* `Mmax` - maximum number of bidirectional connections a node can have
* `select_mode` - method for selecting neighbors during construction

## Usage Examples
Usage examples can be found in the [examples](examples) directory. Some of these examples require external libraries to work such as `hnswlib`, `h5py`, `scikit-learn`, `networkx`, and `matplotlib`.