from setuptools import setup, find_packages

setup(
    name='hnsw_study',
    version='0.1.0',
    packages=find_packages(),
    install_requires=[
        'numpy',
    ],
    description='A library for studying the HNSW algorithm',
    author='Diego Lieberherr',
    python_requires='>=3.6',
)
