import hnsw_study
import hnswlib
import numpy as np
'''
another example compared with the hnswlib implementation
'''
dim = 4
num_elements = 1000
M = 16
ef_constr = 100
ef_search = 20
k = 10

#generate some random data samples
data = np.float32(np.random.random((num_elements, dim)))
ids = np.arange(num_elements)

#create the hnswlib index and builds the graphs
p_ref = hnswlib.Index(space = 'l2', dim = dim)
p_ref.init_index(max_elements = num_elements, ef_construction = ef_constr, M = M)
p_ref.add_items(data, ids)

#create the hnsw_study index and builds the graphs
p = hnsw_study.Index(M = M, ef_construction = ef_constr)
p.add(data)

#generate a query sample
query = np.float32(np.random.random(dim))

#k-nearest neighbor search 
p_ref.set_ef(ef_search)
labels_ref, distances_ref = p_ref.knn_query(query, k = k)

distances, labels = p.search(query, ef=ef_search, k=k)


print("hnswlib")
print("nearest neighbors found:")
print(f"labels: {labels_ref}")
print(f"distances: {distances_ref}")
print()
print("hnsw_study")
print("nearest neighbors found:")
print(f"labels: {labels}")
print(f"distances: {distances}")
print()
p.print_query_path()