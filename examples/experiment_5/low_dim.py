import hnsw_study
import hnswlib
import numpy as np
'''
warning this experiment can take up to several minutes potentially hours
experiment 5 with low dimensionality dataset
'''

#compute score metrics
def compute_metrics(pred_labels, true_neighbors, k=10):
    total_instances = pred_labels.shape[0] * pred_labels.shape[1]

    correct_neighbors = 0
    for i in range(pred_labels.shape[0]):
        correct_neighbors += np.intersect1d(pred_labels[i], true_neighbors[i][:k]).size

    return correct_neighbors / total_instances

dim = 3
num_elements = 10000
M = 16
ef_constr = 200
ef_search = 50
k = 10

train_data = np.float32(np.random.random((num_elements, dim)))

test_data = np.float32(np.random.random((num_elements//10, dim)))

hnsw_index = hnswlib.Index(space='l2', dim=dim)
hnsw_index.init_index(max_elements=num_elements, ef_construction=ef_constr, M=M)
hnsw_index.add_items(train_data)

#brute force index to get true nearest neighbors
bf_index = hnswlib.BFIndex(space='l2', dim=dim)
bf_index.init_index(max_elements=num_elements)
bf_index.add_items(train_data)


p = hnsw_study.Index(M = M, ef_construction = ef_constr)
p.add(train_data)

hnsw_index.set_ef(ef_search)

pred_labels_ref, _ = hnsw_index.knn_query(test_data, k=k)
true_neighbors, _ = bf_index.knn_query(test_data, k=k)
pred_labels = np.zeros((test_data.shape[0],k))

for i in range(len(test_data)):
    _, labels = p.search(test_data[i], ef=ef_search, k=k)
    pred_labels[i] = np.array(labels)

score_ref = compute_metrics(pred_labels_ref, true_neighbors)
score = compute_metrics(pred_labels, true_neighbors)


print("hnswlib score ", score_ref)
print("hnsw_study score ", score)

