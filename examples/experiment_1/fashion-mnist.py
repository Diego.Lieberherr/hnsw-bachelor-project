import hnsw_study
import hnswlib
import h5py
import numpy as np
'''
warning this experiment can take up to several minutes potentially hours
experiment 1 with fashion-mnist dataset
'''

#load data and get train data, test data and true neighbors
def load_hdf5(file_path):
    with h5py.File(file_path, 'r') as f:
        train_data = f['train'][:]
        test_data = f['test'][:]
        true_neighbors = f['neighbors'][:]

    return train_data, test_data, true_neighbors

#compute score metric
def compute_metrics(pred_labels, true_neighbors):
    total_instances = pred_labels.shape[0] * pred_labels.shape[1]

    correct_neighbors = 0
    for i in range(pred_labels.shape[0]):
        correct_neighbors += np.intersect1d(pred_labels[i], true_neighbors[i][:k]).size

    return correct_neighbors / total_instances

file_path = '../../datasets/fashion-mnist-784-euclidean.hdf5'

train_data, test_data, true_neighbors = load_hdf5(file_path)

dim = train_data.shape[1]
num_elements = train_data.shape[0]
M = 16
ef_constr = 200
ef_search = 50
k = 10

hnsw_index = hnswlib.Index(space='l2', dim=dim)
hnsw_index.init_index(max_elements=num_elements, ef_construction=ef_constr, M=M)
hnsw_index.add_items(train_data)


p = hnsw_study.Index(M = M, ef_construction = ef_constr)
p.add(train_data)

hnsw_index.set_ef(ef_search)

pred_labels_ref, _ = hnsw_index.knn_query(test_data, k=k)
pred_labels = np.zeros((test_data.shape[0],k))

for i in range(len(test_data)):
    _, labels = p.search(test_data[i], ef=ef_search, k=k)
    pred_labels[i] = np.array(labels)

score_ref = compute_metrics(pred_labels_ref, true_neighbors)
score = compute_metrics(pred_labels, true_neighbors)


print("hnswlib score ", score_ref)
print("hnsw_study score ", score)