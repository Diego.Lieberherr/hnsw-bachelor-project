import hnsw_study
import hnswlib
import h5py
import numpy as np
'''
warning this experiment can take up to several minutes potentially hours
experiment 1 with sift dataset
'''
#load data and get train data, test data and true neighbors. Only take a subset of the dataset
def load_hdf5(file_path, train_size=None, test_size=None):
    with h5py.File(file_path, 'r') as f:
        train_data = f['train'][:]
        test_data = f['test'][:]
        true_neighbors = f['neighbors'][:]

    if train_size is not None:
        train_data = train_data[:train_size]
    if test_size is not None:
        test_data = test_data[:test_size]
        true_neighbors = true_neighbors[:test_size]

    return train_data, test_data, true_neighbors

#compute score metric
def compute_metrics(pred_labels, true_neighbors):
    total_instances = pred_labels.shape[0] * pred_labels.shape[1]

    correct_neighbors = 0
    for i in range(pred_labels.shape[0]):
        correct_neighbors += np.intersect1d(pred_labels[i], true_neighbors[i][:k]).size

    return correct_neighbors / total_instances

file_path = '../../datasets/sift-128-euclidean.hdf5'

train_size = 100000
test_size = 10000

train_data, test_data, true_neighbors = load_hdf5(file_path, train_size=train_size, test_size=test_size)

dim = train_data.shape[1]
num_elements = train_data.shape[0]
M = 16
ef_constr = 200
ef_search = 50
k = 10

valid_indices = [i for i, row in enumerate(true_neighbors) if np.sum(row < train_size) >= k]

true_neighbors = true_neighbors[valid_indices]
test_data = test_data[valid_indices]
true_neighbors = np.array([row[row < train_size][:k] for row in true_neighbors])

hnsw_index = hnswlib.Index(space='l2', dim=dim)
hnsw_index.init_index(max_elements=num_elements, ef_construction=ef_constr, M=M)
hnsw_index.add_items(train_data)


p = hnsw_study.Index(M = M, ef_construction = ef_constr)
p.add(train_data)

hnsw_index.set_ef(ef_search)

pred_labels_ref, _ = hnsw_index.knn_query(test_data, k=k)
pred_labels = np.zeros((test_data.shape[0],k))

for i in range(len(test_data)):
    _, labels = p.search(test_data[i], ef=ef_search, k=k)
    pred_labels[i] = np.array(labels)

score_ref = compute_metrics(pred_labels_ref, true_neighbors)
score = compute_metrics(pred_labels, true_neighbors)


print("hnswlib score ", score_ref)
print("hnsw_study score ", score)
