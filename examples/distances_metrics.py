import numpy as np
import matplotlib.pyplot as plt
'''
An example showing the effect of the dimensionality on distance metrics (squared L2 and cosine similarity)
'''
num_elements = 1000
values = np.zeros((1000,7))


for dim in range(1,1001):

    data = np.float32(np.random.random((num_elements, dim)))
    query = np.float32(np.random.random(dim))

    nearestl2 = np.dot(data[0]-query, data[0]-query)
    farthestl2 = nearestl2
    suml2 = 0

    nearestcos = 1.0 - (np.dot(data[0],query)/(np.linalg.norm(data[0])*np.linalg.norm(query)))
    farthestcos = nearestcos
    sumcos = 0


    for n in data:
        diff = n - query
        dist = np.dot(diff, diff)
        if dist < nearestl2:
            nearestl2 = dist
        if dist > farthestl2:
            farthestl2 = dist
        suml2 += dist

        dist = 1.0 - (np.dot(n,query)/(np.linalg.norm(n)*np.linalg.norm(query)))
        if dist < nearestcos:
            nearestcos = dist
        if dist > farthestcos:
            farthestcos = dist
        sumcos += dist
    
    meanl2 = suml2/num_elements
    meancos = sumcos/num_elements

    values[dim-1, 0] = dim
    values[dim-1, 1] = nearestl2
    values[dim-1, 2] = farthestl2
    values[dim-1, 3] = meanl2
    values[dim-1, 4] = nearestcos
    values[dim-1, 5] = farthestcos
    values[dim-1, 6] = meancos

fig, axs = plt.subplots(1,2, figsize=(15, 8))
axs[0].loglog(values[:,0], values[:,1], linestyle='-', marker='.', label = "nearest")
axs[0].loglog(values[:,0], values[:,2], linestyle='-', marker='.', label = "farthest")
axs[0].loglog(values[:,0], values[:,3], linestyle='-', marker='.', label = "mean")
axs[0].set_title('squared L2')
axs[0].set_xlabel('dimension')
axs[0].set_ylabel('distance')
axs[0].grid(which='both', axis='both', ls=":")   
axs[0].legend(loc='lower right')
axs[1].loglog(values[1:,0], values[1:,4], linestyle='-', marker='.', label = "nearest")
axs[1].loglog(values[1:,0], values[1:,5], linestyle='-', marker='.', label = "farthest")
axs[1].loglog(values[1:,0], values[1:,6], linestyle='-', marker='.', label = "mean")
axs[1].grid(which='both', axis='both', ls=":")
axs[1].set_title('cosine similarity')
axs[1].set_xlabel('dimension')
axs[1].set_ylabel('distance')     
axs[1].legend(loc='lower right')  
plt.savefig('dist.png')
plt.close()

plt.plot(values[:,0], values[:,1]/values[:,2], '.', label = "squared L2")
plt.plot(values[1:,0], values[1:,4]/values[1:,5], '.', label = "cosine similarity")
plt.xlabel('dimension')
plt.ylabel('ratio')
plt.grid(which='both', axis='both', ls=":")
plt.legend(loc='lower right')
plt.savefig('ratio.png')  