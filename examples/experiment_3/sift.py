import hnsw_study
import hnswlib
from sklearn.neighbors import kneighbors_graph
import h5py
import numpy as np
'''
warning this experiment can take up to several minutes potentially hours
experiment 3 with sift dataset
'''

#load data and get train data, test data and true neighbors. Only take a subset of the dataset
def load_hdf5(file_path, train_size=None, test_size=None):
    with h5py.File(file_path, 'r') as f:
        train_data = f['train'][:]
        test_data = f['test'][:]
        true_neighbors = f['neighbors'][:]

    if train_size is not None:
        train_data = train_data[:train_size]
    if test_size is not None:
        test_data = test_data[:test_size]
        true_neighbors = true_neighbors[:test_size]

    return train_data, test_data, true_neighbors

#compute score metric
def compute_metrics(pred_labels, true_neighbors):
    total_instances = pred_labels.shape[0] * pred_labels.shape[1]

    correct_neighbors = 0
    for i in range(pred_labels.shape[0]):
        correct_neighbors += np.intersect1d(pred_labels[i], true_neighbors[i][:k]).size

    return correct_neighbors / total_instances

file_path = '../../datasets/sift-128-euclidean.hdf5'

train_size = 100000
test_size = 10000

train_data, test_data, true_neighbors = load_hdf5(file_path, train_size=train_size, test_size=test_size)

dim = train_data.shape[1]
num_elements = train_data.shape[0]
M = 16
ef_constr = 200
ef_search = 50
k = 10

valid_indices = [i for i, row in enumerate(true_neighbors) if np.sum(row < train_size) >= k]

true_neighbors = true_neighbors[valid_indices]
test_data = test_data[valid_indices]
true_neighbors = np.array([row[row < train_size][:k] for row in true_neighbors])

hnsw_index = hnswlib.Index(space='l2', dim=dim)
hnsw_index.init_index(max_elements=num_elements, ef_construction=ef_constr, M=M)
hnsw_index.add_items(train_data)


p = hnsw_study.Index(M = M, ef_construction = ef_constr)
p.add(train_data, build_graph=False)

#build proximity graphs for each layer and manually add them to the index
for graph in p.graphs:
    points = [key for key in graph.data.keys()]
    data_points = np.array([v for v in graph.data.values()])
    if len(points) == 1:
        continue
    elif len(points) <= graph.Mmax:
        knn_graph = kneighbors_graph(data_points, n_neighbors=len(points)-1, mode='connectivity').tocoo()
        for i, j in zip(knn_graph.row, knn_graph.col):
            graph.nodes[points[i]].add(points[j])
    else:
        knn_graph = kneighbors_graph(data_points, n_neighbors=graph.Mmax, mode='connectivity').tocoo()
        for i, j in zip(knn_graph.row, knn_graph.col):
            graph.nodes[points[i]].add(points[j])

hnsw_index.set_ef(ef_search)

pred_labels_ref, _ = hnsw_index.knn_query(test_data, k=k)
pred_labels = np.zeros((test_data.shape[0],k))

for i in range(len(test_data)):
    _, labels = p.search(test_data[i], ef=ef_search, k=k)
    pred_labels[i] = np.array(labels)

score_ref = compute_metrics(pred_labels_ref, true_neighbors)
score = compute_metrics(pred_labels, true_neighbors)


print("hnswlib score ", score_ref)
print("hnsw_study score ", score)

