import hnsw_study
import hnswlib
from sklearn.datasets import make_blobs
import numpy as np
'''
an example with clustered data
'''
dim = 4
num_elements = 1000
M = 16
ef_constr = 100
ef_search = 20
k = 10

#generate random data with 3 clusters
samples, _ = make_blobs(n_samples=num_elements+1, cluster_std=0.01, n_features=dim, center_box=(0,1))
samples = np.clip(samples, 0, 1)
data = samples[:-1]
ids = np.arange(num_elements)

p_ref = hnswlib.Index(space = 'l2', dim = dim)
p_ref.init_index(max_elements = num_elements, ef_construction = ef_constr, M = M)
p_ref.add_items(data, ids)

p = hnsw_study.Index(M = M, ef_construction = ef_constr)
p.add(data)

query = samples[-1]

p_ref.set_ef(ef_search)
labels_ref, distances_ref = p_ref.knn_query(query, k = k)

distances, labels = p.search(query, ef=ef_search, k=k)


print("hnswlib")
print("nearest neighbors found:")
print(f"labels: {labels_ref}")
print(f"distances: {distances_ref}")
print()
print("hnsw_study")
print("nearest neighbors found:")
print(f"labels: {labels}")
print(f"distances: {distances}")
print()
p.print_query_path()