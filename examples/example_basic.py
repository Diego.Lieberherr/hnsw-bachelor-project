import hnsw_study
import numpy as np
'''
a basic example usage of creating hnsw_study index and search k-nearest neighbors
'''
dim = 10
num_elements = 1000

#generate some random data samples
data = np.float32(np.random.random((num_elements, dim)))

#create the index and builds the hnsw graphs
p = hnsw_study.Index(M = 16, ef_construction = 100)
p.add(data)

#generate a query sample
query = np.float32(np.random.random(dim))

#k-nearest neighbor search 
distances, labels = p.search(query, ef=50, k=5)

print("nearest neighbors found:")
print(f"labels: {labels}")
print(f"distances: {distances}")
print()

p.print_query_path()