import hnsw_study
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
'''
an example to visualize hnsw graph creation
'''
#dim must be fixed at 2 for the nodes to be positioned based on their attributes
dim = 2
num_elements = 100

data = np.float32(np.random.random((num_elements, dim)))


p = hnsw_study.Index(M = 8, ef_construction = 50)
p.add(data)

query = np.float32(np.random.random(dim))

n_graphs = len(p.graphs)

fig, axs = plt.subplots(1,n_graphs, figsize=(n_graphs*10, 10))

for l in range(n_graphs):
    G = nx.Graph()
    edges = []
    for node, neighbors in p.graphs[l].nodes.items():
        for neighbor in neighbors:
            edges += [(node, neighbor)]

    G.add_nodes_from(p.graphs[l].data.keys())
    G.add_edges_from(edges)
    pos = {k: tuple(v) for k, v in p.graphs[l].data.items()}
    color_map = [len(v) for _, v in p.graphs[l].nodes.items()]

    nx.draw_networkx(G, pos=pos, node_color=color_map, cmap=plt.cm.winter, ax=axs[l])

    G.add_node(num_elements)
    pos[num_elements] = tuple(query)

    nx.draw_networkx(G, nodelist=[num_elements], pos=pos, node_color='red', ax=axs[l], labels={num_elements: 'q'})

    axs[l].set_xlim(-0.1,1.1)
    axs[l].set_ylim(-0.1,1.1)
    axs[l].tick_params(left=True, bottom=True, labelleft=True, labelbottom=True)
    axs[l].set_title(f'layer {l}')

distances, labels = p.search(query, ef=10, k=5)

print("nearest neighbors found:")
print(f"labels: {labels}")
print(f"distances: {distances}")
print()

p.print_query_path()

plt.savefig("graphs.png")


